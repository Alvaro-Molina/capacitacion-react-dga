import React from "react";
import "./styles.css";
import TextFieldForm from "./TextFieldform";

class Formulario extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nombre: "",
      apellido: "",
      edad: "",
      sexo: "",
      estadoCivil: ""
    };
  }

  handleChange = event => {
    const name = event.target.name;
    let value = event.target.value;

    this.setState({ [name]: value });
  };

  handleSubmit = event => {
    alert("A name was submitted: " + this.state[event.target.name]);
    event.preventDefault();
  };

  render() {
    const { nombre, apellido, edad, sexo, estadoCivil } = this.state;
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <p>
            <label>
              Nombre:{" "}
              <TextFieldForm nombre="nombre" onChange={this.handleChange} />
            </label>
            <br />
            <br />

            <label>
              Apellido:{" "}
              <TextFieldForm nombre="apellido" onChange={this.handleChange} />
            </label>
            <br />
            <br />
            <label>
              Edad:
              <input
                type="number"
                name="edad"
                min="1"
                onChange={this.handleChange}
              />
            </label>
            <br />
            <label>
              Sexo:
              <div className="radio">
                <input
                  type="radio"
                  name="sexo"
                  value="Masculino"
                  onChange={this.handleChange}
                />{" "}
                Masculino
                <br />
                <input
                  type="radio"
                  name="sexo"
                  value="Femenino"
                  onChange={this.handleChange}
                />{" "}
                Femenino
                <br />
              </div>
            </label>
            <br />
            <br />
            <label>
              Estado Civil:
              <select name="estadoCivil" onChange={this.handleChange}>
                <option value="Casado">Casado</option>
                <option value="Soltero" selected>
                  Soltero
                </option>
              </select>
            </label>
            <br />
            <br />
            <input type="submit" value="Submit" />
          </p>
        </form>
        <hr />
        VISUALIZACION DE INFORMACION
        <hr />
        <br />
        <h1 className="Etiqueta" style={{ color: "red" }}>
          Nombre : {nombre}
        </h1>
        <h1 className="Etiqueta" style={{ color: "red" }}>
          Apellido : {apellido}
        </h1>
        <h1 className="Etiqueta" style={{ color: "red" }}>
          {" "}
          Edad : {edad}
        </h1>
        <h1 className="Etiqueta" style={{ color: "red" }}>
          Sexo : {sexo}
        </h1>
        <h1 className="Etiqueta" style={{ color: "red" }}>
          Estado civil : {estadoCivil}
        </h1>
      </div>
    );
  }
}
export default Formulario;
