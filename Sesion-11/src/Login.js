import React from "react";
import Saluda from "./Saluda";
class Login extends React.Component {
  state = {
    user: "",
    password: "",
    mostrar: false
  };

  onChange = e => {
    const name = e.target.name;
    let value = e.target.value;
    this.setState({ [name]: value });
    console.log("el estado es: ", this.state.user);
  };

  entrar = e => {
    e.preventDefault();
    this.setState({
      mostrar: true
    });
    console.log("hola mundo", this.state.user);
  };
  render() {
    return (
      <div>
        <form style={{ background: "#20B2AA" }}>
          <label>
            Usuario:
            <input onChange={this.onChange} name="user" placeholder="Usuario" />
          </label>

          <br />
          <label>
            Contraseña:
            <input
              type="password"
              onChange={this.onChange}
              name="password"
              placeholder="Contraseña"
            />
          </label>

          <br />
          <button onClick={this.entrar}>Entrar</button>
        </form>
        {this.state.mostrar && (
          <Saluda usuario={this.state.user} password={this.state.password} />
        )}
      </div>
    );
  }
}

export default Login;
