import React from "react";
import { Link, Switch, Route } from "react-router-dom";

export default ({ color, valor }) => {
  return (
    <div style={{ background: color }}>
      {valor} <br />
      <Switch>
        <Route
          exact
          path="/layout/page-b"
          component={() => <Link to="/dashboard">Dashboard</Link>}
        />
      </Switch>
      <h4>Prueba de carga Page B </h4>
    </div>
  );
};
