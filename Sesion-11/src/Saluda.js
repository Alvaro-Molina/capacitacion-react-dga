import React from "react";

class Saluda extends React.Component {
  render() {
    const { usuario, password } = this.props;
    return (
      <div style={{ background: "#98FB98" }}>
        <h2 style={{ textAlign: "center", color: "#808000" }}>
          Datos Usuario ingresado
        </h2>
        <h3 style={{ textAlign: "center", color: "DarkSlateGray" }}>
          Usuario : {usuario}
          <br />
          Contraseña : {password}
        </h3>
      </div>
    );
  }
}

export default Saluda;
