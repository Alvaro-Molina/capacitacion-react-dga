import React from "react";
import { Switch, Route } from "react-router-dom";
import Layout from "./Layout";
import Dashboard from "./Dashboard";
import "./styles.css";

export default function App() {
  return (
    <Switch>
      <Route component={Layout} />
    </Switch>
  );
}

