import React from "react";
import { Link, Switch, Route } from "react-router-dom";
import "./Layout.css";
import Login from "./Login";
import Home from "./Home";
import PageA from "./PageA";
import PageB from "./PageB";

export default () => {
  return (
    <div className="layout">
      <nav className="layout-nav">
        <ul>
          <li>
            <Link to="/layout"> Home </Link>
          </li>
          <li>
            <Link to="/layout/page-a">Page A</Link>
          </li>
          <li>
            <Link to="/layout/page-b">Page B</Link>
          </li>
          <li>
            <Link to="/layout/login">Login</Link>
          </li>
        </ul>
      </nav>

      <div className="layout-content">
        <Switch>
          <Route exact path="/layout">
            <Home color="DarkCyan" valor="Home" />
          </Route>
          <Route path="/layout/page-a">
            <PageA color="#808000" valor="Page A" />
          </Route>
          <Route path="/layout/page-b">
            <PageB color="#BA55D3" valor="Page B" />
          </Route>
          <Route path="/layout/login" component={Login} />
        </Switch>
      </div>
    </div>
  );
};
