import React from "react";

export default ({ color, valor }) => {
  return (
    <div style={{ background: color }}>
      {valor}
      <h4>background color: {color}</h4>
    </div>
  );
};
