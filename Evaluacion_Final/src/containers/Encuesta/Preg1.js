// @vendors
import React from "react";

import { Formik, Field, Form } from "formik";
// @components
import Preguntas from "../../components/Pregunta";

export default () => {
  const [state, setState] = React.useState({
    ops: "",
    val: ""
  });

  function handleChange(evt) {
    const value =
      evt.target.type === "checkbox" ? evt.target.checked : evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value
    });
  }

  return (
    <div>
      <Formik
        initialValues={{
          ops: ""
        }}
        onSubmit={async values => {
          await new Promise(r => setTimeout(r, 500));
          alert(JSON.stringify(state.ops, null, 2));
        }}
      >
        {({ values }) => (
          <div className="layout">
            <div className="layout-content">
              <Form>
                <div>
                  <Preguntas title="¿Hace cuanto tiempo conoce de (React) ?">
                    <div className="radio-buttons">
                      <div role="group" aria-labelledby="my-radio-group">
                        <label>
                          <Field
                            id="op1"
                            value="opcion1"
                            name="ops"
                            type="radio"
                            checked={state.ops === "opcion1"}
                            onChange={handleChange}
                          />
                          opcion 1
                        </label>
                        <br />
                        <label>
                          <Field
                            id="op2"
                            value="opcion2"
                            name="ops"
                            type="radio"
                            checked={state.ops === "opcion2"}
                            onChange={handleChange}
                          />
                          opcion 2
                        </label>
                        <br />
                        <label>
                          <Field
                            id="op3"
                            value="opcion3"
                            name="ops"
                            type="radio"
                            checked={state.ops === "opcion3"}
                            onChange={handleChange}
                          />
                          opcion 3
                        </label>
                      </div>
                    </div>
                  </Preguntas>
                  <div>Respuesta: {state.ops}</div>
                </div>

                <div>
                  {state.ops ? <button type="submit">Siguiente</button> : null}
                </div>
              </Form>
            </div>
          </div>
        )}
      </Formik>
    </div>
  );
};

