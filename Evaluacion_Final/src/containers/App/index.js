// @vendors
import React from "react";
import { Switch, Route } from "react-router-dom";
// @components
import Home from "../Home";
import Preg1 from "../Encuesta/Preg1";
import Login from "../Login";
// @utilities
import ProtectedRoute from "../../utilities/ProtectedRouter";
import "./styles.css";

export const AuthContext = React.createContext();

export default function App() {
  return (
    <Switch>
      {/* <Route path="/layout" component={Layout} /> */}
      <ProtectedRoute path="/">
        <Home />
      </ProtectedRoute>
      <ProtectedRoute path="/home/encuesta/preg1">
        <Preg1 />
      </ProtectedRoute>
      <Route path="/login" component={Login} />
    </Switch>
  );
}