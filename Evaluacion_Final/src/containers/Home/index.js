// @vendors
import React from "react";

import { Link, Switch, Route, useHistory } from "react-router-dom";
// @components
import Preg1 from "../Encuesta/Preg1";
// @utilities
import { useAuth } from "../../utilities/useAuth";
import "./styles.css";

export default () => {
  const history = useHistory();
  const auth = useAuth();

  return (
    <div className="layout">
      <nav className="layout-nav">
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/Home/Encuesta">Encuesta</Link>
          </li>

          <li>
            {auth.isAuthenticated ? (
              <button
                onClick={() => {
                  auth.logout(() => history.push("/login"));
                }}
              >
                Cerrar sesión
              </button>
            ) : (
              <Link to="/login">Iniciar sesión</Link>
            )}
          </li>
        </ul>
      </nav>
      <div className="layout-content">
        <Switch>
          <Route exact path="/" component={() => <p> Home</p>} />
          <Route path="/home/encuesta" component={() => <Preg1 />} />
        </Switch>
      </div>
      <div className="layout-footer">
        <div>Todos los derechos reservados 2020</div>
      </div>
    </div>
  );
};
