import React from "react";

export default class Preguntas extends React.Component {
  render() {
    return (
      <div>
        <h3>{this.props.title}</h3>
        <div>{this.props.children}</div>
      </div>
    );
  }
}
