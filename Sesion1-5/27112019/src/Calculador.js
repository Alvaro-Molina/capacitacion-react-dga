import React from "react";

class Calculadora extends React.Component {
  state = {
    valor: 0,
    primerValor: 0,
    segundoValor: 0
  };
  onChangePrimerValor = e => {
    this.setState({ primerValor: e.target.value });
  };
  onChangeSegundoValor = e => {
    this.setState({ segundoValor: e.target.value });
  };
  sumar = () => {
    const primerValor = this.state.primerValor;
    const segundoValor = this.state.segundoValor;

    const resultado = parseInt(primerValor) + parseInt(segundoValor);
    this.setState({ valor: resultado });
  };
  resta = () => {
    const primerValor = this.state.primerValor;
    const segundoValor = this.state.segundoValor;

    const resultado = parseInt(primerValor) - parseInt(segundoValor);
    this.setState({ valor: resultado });
  };
  multiplicacion = () => {
    const primerValor = this.state.primerValor;
    const segundoValor = this.state.segundoValor;

    const resultado = parseInt(primerValor) * parseInt(segundoValor);
    this.setState({ valor: resultado });
  };

  division = () => {
    const primerValor = this.state.primerValor;
    const segundoValor = this.state.segundoValor;

    const resultado = parseInt(primerValor) / parseInt(segundoValor);
    this.setState({ valor: resultado });
  };

  render() {
    return (
      <div>
        <div className="Pirmer-valor">
          Primer Valor
          <input onChange={this.onChangePrimerValor} />
        </div>
        <div className="Segundo-valor">
          Segundo Valor
          <input onChange={this.onChangeSegundoValor} />
        </div>
        <div className="acciones">
          <button onClick={this.sumar}>+</button>
          <button onClick={this.resta}>-</button>
          <button onClick={this.multiplicacion}>*</button>
          <button onClick={this.division}>/</button>
        </div>
        <div className="resultado">{this.state.valor}</div>
      </div>
    );
  }
}
export default Calculadora;
