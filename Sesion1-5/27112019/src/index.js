import React from "react";
import ReactDOM from "react-dom";

import TextField from "./TextField";
import "./styles.css";
import Saluda from "./Saluda";
import Calculadora from "./Calculador";

const Parrafo = () => <p> Soy el contenido</p>;
const Container = () => (
  <div style={{ color: "red" }}>
    {" "}
    <Parrafo />
  </div>
);

function App() {
  return (
    <div className="App">
      <Calculadora />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
