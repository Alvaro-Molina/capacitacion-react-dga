import React from "react";

class TextField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "Hola"
    };
  }
  onChange = e => {
    const valor = e.target.value;
    this.setState({ value: valor });
  };

  render() {
    return <input value={this.state.value} onChange={this.onChange} />;
  }
}
export default TextField;
