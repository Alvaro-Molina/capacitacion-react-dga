import React from "react";
import ReactDOM from "react-dom";
import Fromulario from "./Fromulario";

import "./styles.css";

function App() {
  return (
    <div className="App">
      <h1>Curso React DGA 2019</h1>
      <h2>Formulario handlechange!</h2>
      <Fromulario />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
