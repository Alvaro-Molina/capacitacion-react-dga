const data = [
    {
      name: "nombre 1",
      children: [
        {
          name: "nombre 1.1",
          children: [
            {
              name: "nombre 1.1.1"
            },
            {
              name: "nombre 1.1.2"
            }
          ]
        },
        {
          name: "nombre 1.2",
          children: [
            {
              name: "nombre 1.2.1"
            },
            {
              name: "nombre 1.2.2"
            }
          ]
        }
      ]
    },
    {
      name: "nombre 2",
      children: [
        {
          name: "nombre 2.1",
          children: [
            {
              name: "nombre 2.1.1"
            },
            {
              name: "nombre 2.1.2"
            }
          ]
        },
        {
          name: "nombre 2.2",
          children: [
            {
              name: "nombre 2.2.1"
            },
            {
              name: "nombre 2.2.2"
            }
          ]
        }
      ]
    }
  ];



//usando  forEach
  function ImprirArbol(data){
   //contar los elementos
    data.forEach(element => {
      //imprime cada elemento
        console.log(element.name);
        // verifica si elemento es children
        if (element.children){
          //envia el elemento children a la funcion para posteriormente imprimirlo
            ImprirArbol(element.children);
        }
        
    }); 

  }
  
  ImprirArbol(data);

  
  