/*Factorial
5*4*3*2*1= 120
*/
//con for
function factorial(n){
 let res=1;
 for (let i = n ; i >= 1 ; i--){
     res = res * i ;
 }
 return res;
}
const fact = factorial(5);
console.log(fact);