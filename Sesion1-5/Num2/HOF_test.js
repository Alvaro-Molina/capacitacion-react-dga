const ops = {
    sumar: function sumarNumeros(n1, n2) {
        return (parseInt(n1) + parseInt(n2));
    },

    restar: function restarNumeros(n1, n2) {
        return (parseInt(n1) - parseInt(n2));
    },
    
    multiplicar: function multiplicarNumeros(n1, n2) {
        return (parseInt(n1) * parseInt(n2));
    },

    dividir: function dividirNumeros(n1, n2) {
        return (parseInt(n1) / parseInt(n2));
    }


};
function operaciones(op,num1,num2)
{
    switch(op) {
        case '+':
            var resultado = ops.sumar(num1, num2);
            console.log ("Suma: "+resultado);
            break;
        case '-':
            var resultado = ops.restar(num1, num2);
            console.log ("Resta: " +resultado);
            break;
        case '*':
            var resultado = ops.multiplicar(num1, num2);
            console.log("Multiplicacion: " +resultado);
            break;
        case '/':
            var resultado = ops.dividir(num1, num2);
            console.log("Division: " +resultado);
            break;

    }
}
operaciones('+',2,5);