const saluda = () =>{
    console.log("Hola mundo");

}
// con return
const sumar = (a,b) => {
    return a+b;
}
// sin return o return explicito
const sumar2 = (a,b) => a + b;

//console.log(saluda())
console.log(sumar2(3,5))