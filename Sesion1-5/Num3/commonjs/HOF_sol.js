// Hacer una función que tome 2 parámetros:
// 1. Tipo de Operacion (multiplicar, dividir, sumar o restar)
// 2. Un objeto que contenga dos claves: a y b con sus respectivos valores, ejemplo:
// { a: 2, b: 1 }

// En base al tipo de operacion retornar la funcion que realizará dicha operación y mostrarlo por consola

function factory(op, data) {
  if (op === "+") {
    return function() {
      return data.a + data.b;
    };
  }

  if (op === "-") {
    return function() {
      return data.a - data.b;
    };
  }

  if (op === "*") {
    return function() {
      return data.a * data.b;
    };
  }

  if (op === "/") {
    return function() {
      return data.a / data.b;
    };
  }
}

const sum = factory("+", { a: 2, b: 2 });
const res = factory("-", { a: 2, b: 2 });
const mul = factory("*", { a: 2, b: 2 });
const div = factory("/", { a: 2, b: 2 });

console.log(sum());
console.log(res());
console.log(mul());
console.log(div());
