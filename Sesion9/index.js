import React from "react";
import ReactDOM from "react-dom";

import "./styles.css";
import ListaTareas from "./ListaTareas";

function App() {
  return (
    <div className="App">
      <ListaTareas />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);