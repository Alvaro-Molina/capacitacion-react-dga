import React from "react";
import TextFieldFrom from "./TextFieldFrom";

import "./styles.css";

class ListaTareas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tareas: "",
      key: 1,
      listaTareas: [{ key: null, valor: null, estado: null }]
    };
  }
  handleChange = event => {
    let value = event.target.value;
    this.setState({ tareas: value });
  };
  handleSubmit = event => {
    if (!this.state.tareas) {
      alert("No haya datos!");
      event.preventDefault();
    } else {
      this.setState({ key: this.state.key + 1 });

      let nuevoItems = [
        {
          key: this.state.key + 1,
          valor: this.state.tareas,
          estado: "Pendiente"
        }
      ];

      this.setState(prevState => ({
        listaTareas: prevState.listaTareas.concat(nuevoItems)
      }));
      event.preventDefault();
    }
  };
  eliminarTarea = key => {
    this.setState({
      listaTareas: this.state.listaTareas.filter(function(tareaKey) {
        return tareaKey.key !== key;
      })
    });
  };
  cambiarEstado = valor => {
    valor.estado = "Realizado";
    let estado = this.state.listaTareas;

    let nuevoEstado = Object.assign({ obj: estado });

    this.setState({ listaTareas: nuevoEstado.obj });
  };
  render() {
    const { tareas, key, listaTareas } = this.state;
    return (
      <div>
        <div>
          <h1>Tareas</h1>
          <form onSubmit={this.handleSubmit}>
            <p>
              <label>
                Tarea:
                <TextFieldFrom nombre="tareas" onChange={this.handleChange} />
              </label>
              <br />
              <input type="submit" value="Agregar Tareas" />
            </p>
          </form>
          <h1>Lista de Tareas</h1>
          <br />
          {this.state.listaTareas.map(valor => (
            <div>
              {valor.lenght !== 0 && valor.valor}
              {valor.valor !== null && (
                <button onClick={() => this.cambiarEstado(valor)}>
                  {valor.estado}
                </button>
              )}
              {valor.valor !== null && (
                <button onClick={() => this.eliminarTarea(valor.key)}>
                  Eliminar
                </button>
              )}
            </div>
          ))}
          <br />
        </div>
      </div>
    );
  }
}
export default ListaTareas;